function plv_lower = PLV_l(VLV0, VLV, elad, elcx, En, Ei, R)
    A = 0.17*7.5*10;
    B = 0.015*1.0;
    Vd = 5;
    Pedlcx = (1-elcx)*A*(exp(B*(1-R)*(VLV-Vd))-1);
    Pedlad = (1-elad)*A*(exp(B*R*(VLV-Vd)-1));
    plv_lower = (VLV-VLV0)*En*Ei*elad*elcx/(Ei*(elcx+eps)*(R)+(1-R)*En*(elad+eps))+((1-R)*Pedlcx+R*Pedlad);
end
