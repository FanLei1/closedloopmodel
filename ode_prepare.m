function [Pcdot, Rpt, dRpdP]=ode_prepare(t,Pc,network_matrix,n,...
    full_dPTdt,long_dist,fm,SSR,dSSRdt, Part, Pven, IMP)
[PT]=IMP*network_matrix(:,17);
[dPTdt]=full_dPTdt*network_matrix(:,17);
[Pin]=Part;
[Pout]=Pven*network_matrix(:,17);
[Pcdot,Rpt,dRpdP]=...
    matrices_AandB(t,PT,dPTdt,Pin,Pout,Pc,network_matrix,long_dist,n,fm);





