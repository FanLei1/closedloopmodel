function [tau_lumped,pr_gd_avg,tau_avg,Q_avg,deltaP,deltaP_avg,Pa_i,Pv_i] = ...
    shear_stress_flow_press_avg(time,p_lumped,Pa,Pv,PT,flow,Rt,network_matrix)

% Collect the nodal pressures
    Pnode = flow.Pnode;
    
    % Get the number of vessels
    n = length(network_matrix(:,1));
    
    % Interpolate the pressures at the time values obtained by ODE
    Pa_i = fnval(Pa,time-floor(time));
    Pv_i = fnval(Pv,time-floor(time));
    PT1 = fnval(PT,time-floor(time));
    
    % Calculate the average pressures
    Pin_avg = trapz(time,Pa_i);                                                
    Pout_avg = trapz(time,Pv_i);
    %%
    % Calculate shear stress in each vessel 
       
    % Shear stress in the source vessel
    tau_lumped(:,1) = (Pa_i - Pnode(:,1)).*Rt(:,1)./(2*network_matrix(1,2)*1e-3);
    pr_gd(:,1) = Pa_i - Pnode(:,2);
    % Shear stress in the rest of the vessels
    for j = 2:n,
        tau_lumped(:,j)= abs((Pnode(:,network_matrix(j,5))-Pnode(:,network_matrix(j,6))))...
                            .*Rt(:,j)./(2*network_matrix(j,2)*1e-3);
        pr_gd(:,j) = abs((Pnode(:,network_matrix(j,5))-Pnode(:,network_matrix(j,6)))); 
    end
    
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % Average pressure, flow and shear stress
   
    for i = 1:n,
      deltaP(:,i) = p_lumped(:,i) - PT1; 
      pr_gd_avg(i) = trapz(time,pr_gd(:,i));
      tau_avg(i) = trapz(time,tau_lumped(:,i));
      Q_avg(i) = trapz(time,flow.flow_out(:,i));
      deltaP_avg(i) = trapz(time,deltaP(:,i));
    end
    deltaP_avg = deltaP_avg';
    tau_avg = tau_avg';


