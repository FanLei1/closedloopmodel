function [flow]= check_delta_flow_sim(time,vessels_pressure,deltaP,...
    network_matrix,n,Rpt,dRpdP,Part,Pven, dP_dt)
node_vector = unique(network_matrix(:,5:6));
nodes = max(node_vector);
nm = zeros(nodes, 4);
for i = 1:nodes
    [row,col] = find(network_matrix(:,5:6)==i);
    nm(i,1:size(row)) = row';
end
flow.Pnode = zeros(1,nodes);
vessels(1)=find(network_matrix(:,7)==0);
vessels(2:length(find(network_matrix(:,9)==0))+1)=find(network_matrix(:,9)==0);
viscosity_i=2.7e-9;
P(1,:) = vessels_pressure(length(time),:);
G_vessels(1,:) = (pi*Rpt(length(time),:).^4)./(8*viscosity_i.*network_matrix(:,2)'*1e-3);
C_vessels(1,:) = 2*pi*1e-3*network_matrix(:,2)'.*Rpt(length(time),:).*dRpdP(length(time),:);
Pin = Part;
Pout = Pven;
flow.Pnode(:,1) = Pin';
for j = 2:nodes
    row = nonzeros(nm(j,:));
    if(size(row,1)==1)
        flow.Pnode(:,j)=Pout';
    elseif(size(row,1)==2)
        flow.Pnode(:,j) = (P(:,nm(j,1)).*G_vessels(:,nm(j,1))+P(:,nm(j,2)).*G_vessels(:,nm(j,2)))./ ...
            (G_vessels(:,nm(j,1))+G_vessels(:,nm(j,2)));
    elseif(size(row,1)==3)
        flow.Pnode(:,j)=(P(:,nm(j,1)).*G_vessels(:,nm(j,1))+P(:,nm(j,2)).*G_vessels(:,nm(j,2))+ ...
            P(:,nm(j,3)).*G_vessels(:,nm(j,3)))./(G_vessels(:,nm(j,1))+G_vessels(:,nm(j,2))+G_vessels(:,nm(j,3)));
    else
        flow.Pnode(:,j)=(P(:,nm(j,1)).*G_vessels(:,nm(j,1))+P(:,nm(j,2)).*G_vessels(:,nm(j,2))+ ...
            P(:,nm(j,3)).*G_vessels(:,nm(j,3))+P(:,nm(j,4)).*G_vessels(:,nm(j,4)))./ ...
            (G_vessels(:,nm(j,1))+G_vessels(:,nm(j,2))+G_vessels(:,nm(j,3))+G_vessels(:,nm(j,4)));
    end
end
for j = 2:nodes
    row = nonzeros(nm(j,:));
    if(size(row,1)==2)
        flow.flow_in_node(:,j)=2*(P(:,nm(j,2))-flow.Pnode(:,j)).*G_vessels(:,nm(j,2));
        flow.flow_out_node(:,j)=2*(flow.Pnode(:,j)-P(:,nm(j,1))).*G_vessels(:,nm(j,1));
        flow.net_flow_node(:,j) = (flow.flow_in_node(:,j) - flow.flow_out_node(:,j))*100./flow.flow_in_node(:,j);
    elseif(size(row,1)==3)
        flow.flow_in_node(:,j)=2*(P(:,nm(j,3))-flow.Pnode(:,j)).*G_vessels(:,nm(j,3));
        flow.flow_out_node(:,j)=2*(flow.Pnode(:,j)-P(:,nm(j,1))).*G_vessels(:,nm(j,1))+...
            2*(flow.Pnode(:,j)-P(:,nm(j,2))).*G_vessels(:,nm(j,2));
        flow.net_flow_node(:,j) = (flow.flow_in_node(:,j) - flow.flow_out_node(:,j))*100./flow.flow_in_node(:,j);
    elseif(size(row,1)==4)
        flow.flow_in_node(:,j)=2*(P(:,nm(j,4))-flow.Pnode(:,j)).*G_vessels(:,nm(j,4));
        flow.flow_out_node(:,j)=2*(flow.Pnode(:,j)-P(:,nm(j,1))).*G_vessels(:,nm(j,1))+ ...
            2*(flow.Pnode(:,j)-P(:,nm(j,2))).*G_vessels(:,nm(j,2))+...
            2*(flow.Pnode(:,j)-P(:,nm(j,3))).*G_vessels(:,nm(j,3));
        flow.net_flow_node(:,j) = (flow.flow_in_node(:,j) - flow.flow_out_node(:,j))*100./flow.flow_in_node(:,j);
    end 
end
flow.flow_in(:,:)= 2*(flow.Pnode(:,network_matrix(:,5))-P(:,:)).*G_vessels(:,:);
flow.flow_out(:,:) = 2*(P(:,:)-flow.Pnode(:,network_matrix(:,6))).*G_vessels(:,:) + C_vessels(:,:).*dP_dt(:,:);
