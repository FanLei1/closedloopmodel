function [Rpt, dRpdP] = passive_radius(t,deltaP,long_dist,n)

Rpt=zeros(n,1);
dRpdP=zeros(n,1);

Rpt = long_dist.bp + (long_dist.ap-long_dist.bp)/pi.*(pi/2 + ...
    atan((deltaP-long_dist.php)./long_dist.cp));

dRpdP = (long_dist.ap - long_dist.bp)./(pi*long_dist.cp.*(1.0 + ...
    ((deltaP - long_dist.php)./long_dist.cp).^2));


