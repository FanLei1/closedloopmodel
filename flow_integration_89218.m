function [Qnlad, Qnlcx] = flow_integration_89218(last_t, BCL, ts, FLOWLAD, FLOWLCx)
t1 = find(ts < last_t+BCL);
plotind = find(ts(t1) > last_t);
Qnlad = 0;
Qnlcx = 0;
deltat = 0.6012/158;    
for j = 1:158
    m = plotind(j);
    Qnlad = Qnlad+(FLOWLAD(m)+FLOWLAD(m+1))*deltat/2;
    Qnlcx = Qnlcx+(FLOWLCx(m)+FLOWLCx(m+1))*deltat/2;
end
