%% FLOW CONTROL: Main script to solve flow control in a network of 400 blood vessels  
%
%  Inputs: pa_pv_file - file of pa and pv (columns:time (non-
%                      dimensionalised), pa (mmHg), pv (mmHg))
%         pt_file - file of pt (columns: time (non-dimensionalised),
%                   pt (mmHg))
%         network matrix - columns: columns: vessel no., length (micron),
%                          diameter (micron), alpha (mmHg), 
%              inlet node, outlet node, parent, sister, daughter 1, daughter 2
%                          (alpha and diameter are redundant...need to be
%                          removed)
%         
% Returns: 1. graphs of pasive and active vessel radii versus average pressure 
%          2. graph of net flow at nodes in the vessel and the junction of three vessel 
%          3. graph of shear stress vs average pressure
%          4. graph of input and out pressures (initial mother & terminating
%          daughter vessels)
%          5. graph of active and passive tensions in vessel wall
%          6. graph of active and passive stiffness of vessel wall
%
% Architecture of function calls  
% 1. ODE_PassiveFlow.m -> ode15s.m -> ode_prepare.m -> matrices_AandB.m 
%                                                        -> check_viscosity.m
%                                                        -> passive_radius.m
%       -> check_delta_flow.m
% 2. steady_radius_activation.m
% 3. active_tension.m
% 4. passive_stiffness.m
% 5. active_stiffness.m
% 6. ODE_ActiveFlow.m
% Author: Ravi Namani


%% Initialize variables and load inputs

function[flow, p_lumped, Q] = FlowControl_vessel_LAD(Part, Pven, t1, t2, p_lump0, IMP, IMP0, step,SSR, dSSRdt)

network_matrix = load('network_matrix_test_400vessel.txt'); 
n=length(network_matrix(:,1));
[long_dist]=VesselParams1(network_matrix,n);
%% Initialize network variables%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mo = network_matrix(:,7);         % mother index
si = network_matrix(:,8);         % sister1
si2 = network_matrix(:,9);        % sister2
da1 = network_matrix(:,12);       % daughter1
da2 = network_matrix(:,13);       % daughter2
da3 = network_matrix(:,14);       % daughter3 if any
fm.Gnt_loc=find(da1>0 & mo>0);    % Non terminal vessels
fm.Gt_loc = find(da1==0);         % Terminal vessels
fm.mo = mo(fm.Gnt_loc);           % non-terminal vessels' mothers' index          % 
fm.si = si(fm.Gnt_loc);           % non-terminal vessels' sister1' index
fm.si2 = find(si2(fm.Gnt_loc)>0); % find veseels who have sister2
fm.si2index = nonzeros(si2(fm.Gnt_loc));
fm.da1 = da1(fm.Gnt_loc);         % non-terminal vessels' daughter1' index
fm.da2 = da2(fm.Gnt_loc);         % non-terminal vessels' daughter2' index
fm.da3 = find(da3(fm.Gnt_loc)>0); % find vessels who have daughter3
fm.da3index = nonzeros(da3(fm.Gnt_loc)); % find the nonzero daughter3
fm.mo_t = mo(fm.Gt_loc);          % terminal vessels' mothers' index
fm.si_t = si(fm.Gt_loc);          % terminal vessels' sister1' index
fm.si2_t = find(si2(fm.Gt_loc)>0);% terminal vessels' who have sister2
fm.si2tindex = nonzeros(si2(fm.Gt_loc));
dPTdt = (IMP-IMP0)/(t2-t1);
clear time p_lumped deltaP tau_lumped Q_lumped Rpt dRpdP;
[p_lumped,flow] = ODE_PassiveFlow(network_matrix,long_dist,fm,SSR,dSSRdt,Part,Pven,t1,t2,p_lump0,IMP,dPTdt,step);
Q = flow.flow_in(1,1);
