 function [Rss,Rm,A,ftau, fa]= steady_radius_activation(deltaP_avg,tau_avg,Ad,network_matrix,n,long_dist)

  %fprintf('tau_avg       delP_avg        ftau       fa       Adenosine   Activation Rss\n');
Rm = zeros(n,1);
ftau = zeros(n,1);
fa = zeros(n,1);
A = zeros(n,1);
Rss = zeros(n,1);

%Myogenic radius
Rm = (long_dist.rhoa/pi).*( 0.5*pi - atan(((deltaP_avg - ...
    long_dist.pha )./long_dist.ca ).^(2.0*long_dist.ma)));

% Calculate Activation level

%ftau(i) = fmax(i).*((tau_avg(i)./ktau(i)).^5)./(1 + (tau_avg(i)/ktau(i)).^5);
%A = (1 - ftau)*ka;
ftau = long_dist.fmax.*tau_avg./(long_dist.ktau + tau_avg);
%ftau(i) = 0.1;
fa = ((Ad./long_dist.ka).^long_dist.a)./(1 + (Ad./long_dist.ka).^long_dist.a);
A = ((1 - ftau) +(1-fa))/2;

% Average radius at steady state
Rss = long_dist.bp + 1/pi*(long_dist.ap-long_dist.bp).*...
    (pi/2 + atan((deltaP_avg-long_dist.php)./long_dist.cp)) - A.*Rm;

for i=1:n,
    if (network_matrix(i,4)<1) % If vessels order is less than 0
        Rm(i)=0;   
        ftau(i)=0;
        fa(i)=0;
        A(i)=0;
        Rss(i)= long_dist.bp(i) + 1/pi*(long_dist.ap(i)-long_dist.bp(i)).*...
             (pi/2 + atan((deltaP_avg(i)-long_dist.php(i))./long_dist.cp(i)));
    end
end

