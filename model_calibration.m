close all
t0 = 1.6;
ta = find(ts < t0 + BCL);
tb = find(ts(ta) > t0);
a = 0.6012/BCL;
b = -t0*0.6012/BCL;
a = 0.6/BCL;
b = -t0*0.6/BCL;
ay = 1.66;
by = ay/dt;
ty = [by:1:by+159];
tyy = [by+160:1:by+160*2-1];

load PLV_E.mat
load t.mat
figure(1)
plot(ts(tb)*a+b,PLV_lower(ty), 'k-','LineWidth', 2);
hold on 
plot(t,PLV_E, 'k*','markersize',6,'LineWidth', 1);
xlabel({'Time (s)'},'FontSize',24,'Interpreter','latex')
ylabel({'$P_{LV}$ (mmHg)'},'FontSize',24,'Interpreter','latex')
h=legend({'Num.','Exp.'},'FontSize',24,'Interpreter','latex');
set(h,'Box','off');
axis([0 0.7 0 140]);

load QLCX_E.mat
figure(2)
plot(ts(tb)*a+b,FLOWLCx(ty), 'k-','LineWidth', 2);
hold on
plot(t,QLCX_E, 'k*','markersize',6,'LineWidth', 1);
xlabel({'Time (s)'},'FontSize',24,'Interpreter','latex')
ylabel({'$q_{LCX}$ (ml/min)'},'FontSize',24,'Interpreter','latex')
h=legend({'Num.','Exp.'},'FontSize',24,'Interpreter','latex');
set(h,'Box','off');
axis([0 0.7 -30 120]);

load QLAD_E.mat
figure(3)
plot(ts(tb)*a+b,FLOWLAD(ty), 'k-','LineWidth', 2);
hold on
plot(t,QLAD_E,  'k*','markersize',6,'LineWidth', 1);
xlabel({'Time (s)'},'FontSize',24,'Interpreter','latex')
ylabel({'$q_{LAD}$ (ml/min)'},'FontSize',24,'Interpreter','latex')
h=legend({'Num.','Exp.'},'FontSize',24,'FontWeight','bold','Interpreter','latex');
set(h,'Box','off');
axis([0 0.7 -30 120]);

load VLV_E.mat
figure(4)

plot(ts(tb)*a+b,VLV_all(ty), 'k-','LineWidth', 2);
hold on
plot(t,VLV_E, 'k*','markersize',6,'LineWidth', 1);
xlabel({'Time (s)'},'FontSize',24,'Interpreter','latex')
ylabel({'$V_{LV}$ (ml)'},'FontSize',24,'Interpreter','latex')
h=legend({'Num.','Exp.'},'FontSize',24,'Interpreter','latex');
set(h,'Box','off');
axis([0 0.7 20 65]);


figure(5)                                                                 
plot(ts(tb)*a+b,IMPLAD(ty),'k:', 'LineWidth', 2)
hold on
plot(ts(tb)*a+b,IMPLCX(ty),'k--', 'LineWidth', 2)
hold on
plot(ts(tb)*a+b,gamma*PLV_lower(ty),'k-', 'LineWidth', 2)
hold on
plot(ts(tb)*a+b,alpha*PLVLAD(ty),'k-','color',[0,0,0]+0.7, 'LineWidth', 2)
hold on
plot(ts(tb)*a+b,alpha*PLVLCx(ty),'k--','color',[0,0,0]+0.7, 'LineWidth', 2)
hold on
plot(ts(tb)*a+b,20*myo_LAD(ty)/0.14,'k-','color',[0,0,0]+0.4, 'LineWidth', 2)
hold on
plot(ts(tb)*a+b,20*myo_LCX(ty)/0.14,'k--','color',[0,0,0]+0.4, 'LineWidth', 2)
h=legend({'{IMP$_{LAD}$}','{IMP$_{LCX}$}','CEP','{VE$_{LAD}$}','{VE$_{LCX}$}','{SIP$_{LAD}$}','{SIP$_{LCX}$}'},'FontSize',18,'Interpreter','latex');
xlabel({'Time (s)'},'FontSize',24,'Interpreter','latex')
ylabel({'$IMP$ (mmHg)'},'FontSize',24,'Interpreter','latex')
set(h,'Box','off');
axis([0 0.7 -5 130]);

