% Computational Biomechanics Lab at Michigan State University ALL RIGHTS RESERVED
% Authors: Lei Fan, Ravi Namani and Lik Chuan Lee
% Permission to use, copy, modify and distribute any part of this code for non-profit educational and research purposes, without fee, and without a written agreement is hereby granted, 
% provided that the above copyright notice. Those desiring to use this code for commercial purposes should contact Computational Biomechanics Lab at Michigan State University.
% This code implements a closed-loop lumped parameter model coupled systemic circulation of the left ventricle and coronary perfusion.
% This code simulates the control case.

tic
close all
clear all
%% parameter setting
dt = 0.005;
ts = linspace(0, 7.5, 7.5/dt);
%% parameters used in lumped model
Rao = 0.10;                                                             
Rven = 0.07251;                                                      
Rper = 0.3;                                                          
Rmv = 0.4440; 
RLAD = 0.159;
RLCx = 2.92;
Cao = 0.4;                                                          
Cven = 7.6; 
Vart0 = 10;                                                              
Vven0 = 20;
Vart = 690;
Vven = 0; 
VLV0  = -7; 
VLV   = 15;
VLA0  = 0; 
VLA   = 0;                                                                   
%% parameters used for time-varying elastance model
para_V_LAD = 0.6; 
En = 3.18;
Ei = 3.18;
BCL = 0.8;                                                
AV = 0.6;
Tmax_LV = 0.42;                                                       
tau_LV = 0.027; 
Tmax_la = 0.125;                                                        
tau_la = 0.025;  
for j = 1:length(ts)
    cyc = floor(ts(j)/BCL);
    t_lv = ts(j)-cyc*BCL;   
    e = elas(t_lv, Tmax_LV, tau_LV); 
    E_lv(j) = e;
end
%% parameters for coronary perfusion
FLOWLAD = 0;
FLOWLCx = 0;
Qcor_LAD = 0;
Qcor_LCx = 0;   
VLV_all = 0;
myo_LAD = 0;
myo_LCX = 0;
alpha = 5;
gamma = 0.8; 
beta = 20;
SDI = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:1:(length(ts))
    cycle = floor(ts(i)/BCL);
    CY(i) = cycle;
	t_cell = ts(i) - cycle*BCL;
    if (t_cell < (BCL - AV))
        t_cell_la = t_cell + AV;
    else
        t_cell_la = t_cell - BCL + AV;
    end
    Part = 1/Cao*(Vart - Vart0); 
    Pven = 1/Cven*(Vven - Vven0);
    if i <= SDI
        elax = 0;
    else
        elax = E_lv(i-SDI*1.0);
        elad = E_lv(i-SDI*0);
    end
    Elcx(i) = elax;
    Elad(i) = E_lv(i);
    VLV_LAD = VLV*para_V_LAD;
    VLAD(i) = VLV_LAD;
    VLV_LCx = VLV*(1-para_V_LAD);
    VLCX(i) = VLV_LCx; 
    Plv_LAD = En*elad;
    Plv_LCx = Ei*elax;
    %% use lower bound to calculate PLV
    plv = PLV_l(VLV0, VLV, E_lv(i-0*SDI), E_lv(i-1*SDI), En, Ei, para_V_LAD);
    PLV_lower(i) = plv;
    e_la = elas(t_cell_la, Tmax_la, tau_la);
    Pla = Pre_la(VLA0, VLA,e_la);
    PLVLAD(i) = Plv_LAD;
    PLVLCx(i) = Plv_LCx;
    PVEN(i) = Pven;
    PART(i) = Part;
    PLA(i) = Pla;   
    t1 = ts(i);
    t2 = ts(i)+dt;
%% coronary perfusion
    IMP_LAD = alpha*Plv_LAD+plv*gamma; 
    IMP_LCx = alpha*Plv_LCx+plv*gamma; 
    IMPLAD(i) = IMP_LAD;
    IMPLCX(i) = IMP_LCx;
    if i == 1
        IMP0_LAD = IMP_LAD;
        IMP0_LCx = IMP_LCx;
        p_lump0_LAD = (Part - (Part - Pven)*ones(1,400)./2)*1.33e-4;
        p_lump0_LCx = (Part - (Part - Pven)*ones(1,400)./2)*1.33e-4;        
    else
        IMP0_LAD = alpha*PLVLAD(i-1)+plv*gamma;
        IMP0_LCx = alpha*PLVLCx(i-1)+plv*gamma;
    end
    if i < 1  
        Pout = 1*Pven-92;
    else
        Pout = 1*PVEN(i-0)-92;
    end
    POUT(i) = Pout;
    if i <= SDI+1
        myo_lad = 1 - 1;
        myo_lcx = 1 - 1;
        dssr_lad = 0;
        dssr_lcx = 0;
    else
        %90.5443
        myo_lad = beta*(52.53 - VLV)/52.53/20*0.14;
        myo_lcx = beta*((52.53*(1-para_V_LAD)-(1-para_V_LAD)*VLV_all(i-SDI-1))/(52.53*(1-para_V_LAD)))/20*0.14;
        dssr_lad = (myo_lad-myo_LAD(i-1))/dt;
        dssr_lcx = (myo_lcx-myo_LCX(i-1))/dt;
    end
    IMP_LAD = IMP_LAD+myo_lad;
    IMP_LCX = IMP_LCx+myo_lcx;
    Pin_LAD = Part-Qcor_LAD*RLAD;
    Pin_LCx = Part-Qcor_LCx*RLCx;
    [flow_LAD, p_lump_LAD, Qcor_LAD] = FlowControl_vessel_LAD(Pin_LAD*1.33e-4, Pout*1.33e-4, t1, t2, p_lump0_LAD, IMP_LAD*1.33e-4, IMP0_LAD*1.33e-4, i,myo_lad,dssr_lad);
    [flow_LCx, p_lump_LCx, Qcor_LCx] = FlowControl_vessel(Pin_LCx*1.33e-4, Pout*1.33e-4, t1, t2, p_lump0_LCx, IMP_LCx*1.33e-4, IMP0_LCx*1.33e-4, i,myo_lcx,dssr_lcx);
    myo_LAD(i) = myo_lad;               
    myo_LCX(i) = myo_lcx;
    dSSRLAD(i) = dssr_lad;
    dSSRLCx(i) = dssr_lcx;
    np_LAD = size(p_lump_LAD,1);                                           
    np_LCx = size(p_lump_LCx,1);
    p_lump0_LAD = p_lump_LAD(np_LAD,:);
    p_lump0_LCx = p_lump_LCx(np_LCx,:);
%% lumped model
    if(plv <= Part)
        Qao = 0;
    else
        Qao = 1/Rao*(plv - Part);
    end  
    if(plv >= Pla)
        Qla = 0;
    else
        Qla = 1/Rmv*(Pla - plv);
    end
    Qper = 1/Rper*(Part - Pven);
    Qmv = 1/Rven*(Pven - Pla);
    VLV = VLV + dt*(Qla-Qao);
    VLV_all(i) = VLV;
    VLA = VLA + dt*(Qmv - Qla);
    Vart = Vart + dt*(Qao-Qper-Qcor_LAD-Qcor_LCx);
    Vven = Vven + dt*(Qper-Qmv-Qcor_LAD-Qcor_LCx);
    V_all(i) = VLV + VLA + Vart + Vven;                                              
    FLOWLAD(i) = Qcor_LAD*960;
    FLOWLCx(i) = Qcor_LCx*1020-20;  
end
toc