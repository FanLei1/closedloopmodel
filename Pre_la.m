% function Pla = Pre_la(e, EesLA, VLA, VLA0, ALA, BLA)
%     PesLA = EesLA*(VLA-VLA0);
%     PedLA = ALA*(exp(BLA*(VLA-VLA0))-1);
%     Pla = e*PesLA*VLA+(1-e)*PedLA*VLA;
% end

function Pla = Pre_la(VLA0, VLA, e)
    EesLA = 200*0.0075;
    ALA = 58.67*0.00075;
    BLA = 0.049;
%    EesLA = 200*0.0075;
%     ALA = 58.67*0.0075;
%     BLA = 0.01;
    VLAd0 = 10;
    Pla = e.*EesLA.*(VLA - VLA0) + (1 - e).*ALA.*(exp(BLA*(VLA - VLAd0) - 1)); 
    %= e* EesLA*(VLA - VLA0) + (1 - e)*ALA.*(exp(BLA*(VLA - VLA0) - 1));
end