function e = elas(t, Tmax, tau)
    e = 0.5*(sin((pi/Tmax)*t - pi/2) + 1).*(t <= 1.5*Tmax) + 0.5*exp((-t + (1.5*Tmax))/tau).*(t > 1.5*Tmax);
end
