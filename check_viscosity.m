function [viscosity]=check_viscosity(network_matrix)
viscosity=ones(size(network_matrix,1),1)*2.7e-9;   
