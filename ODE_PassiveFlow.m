
function [vessels_pressure, flow] = ...
    ODE_PassiveFlow(network_matrix,long_dist,fm,SSR,dSSRdt, Part, Pven, t1, t2, p_lump0, IMP,dPTdt,step)
n=size(network_matrix,1); 
vessel_end_pressure= p_lump0;
time_span = [t1 t2];
tolerance=1e-3; 
options=odeset('RelTol',tolerance*1e-3,'AbsTol',tolerance*ones(n,1),'BDF','on','JPattern',jpattern(network_matrix,n)); % 'OutputFcn',@odeplot);
    clear time vessels_pressure

    initial_pressure=vessel_end_pressure; 
    [time,vessels_pressure]= ...
        ode15s(@ode_prepare,time_span,initial_pressure,options,network_matrix,...
        n,dPTdt,long_dist,fm,SSR,dSSRdt, Part, Pven, IMP);
    for i = 1:length(time)
        [dPcdt(i,:), Rpt(i,:), dRpdP(i,:)]= ...
            ode_prepare(time(i,:),vessels_pressure(i,:)',...
            network_matrix,n,dPTdt,long_dist,fm,SSR,dSSRdt, Part, Pven, IMP);
    end
    PT1=IMP*network_matrix(:,17);
    deltaP(:,:) = vessels_pressure(length(time),:) - PT1';
    DELTAP(step,:) = deltaP;
    if  step == 1
        deltaP0 = deltaP;
    else
        deltaP0 = DELTAP(step-1,:);
    end
    dP_dt = (deltaP-deltaP0)/(t2-t1);
    [flow]=...
        check_delta_flow_sim(time,vessels_pressure,deltaP,network_matrix,n,Rpt,...
        dRpdP,Part,Pven,dP_dt);

function S = jpattern(nm,n)
Jac = zeros(n,n);
Jac(1,1) = 1;
Jac(1,nm(1,12))=1;
Jac(1,nm(1,13))=1;
for i=2:n
    if (nm(i,8)~=0 && nm(i,9)==0 && nm(i,12)==0 && nm(i,13)==0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,8))=1;
    elseif (nm(i,9)~=0 && nm(i,12)==0 && nm(i,13)==0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,8))=1;
        Jac(i,nm(i,9))=1;
    elseif(nm(i,8)==0 && nm(i,12)==0 && nm(i,13)==0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
    elseif(nm(i,8)==0 && nm(i,12)==0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
    elseif(nm(i,8)==0 && nm(i,13)==0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,12))=1;
    elseif(nm(i,8)==0 && nm(i,12)~=0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,12))=1;
        Jac(i,nm(i,13))=1;
    elseif(nm(i,8)~=0 && nm(i,13)==0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,8))=1;
        Jac(i,nm(i,12))=1;
    elseif(nm(i,14)~=0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,8))=1;
        Jac(i,nm(i,12))=1;
        Jac(i,nm(i,13))=1;
        Jac(i,nm(i,14))=1;
    elseif(nm(i,9)~=0)
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,8))=1;
        Jac(i,nm(i,9))=1;
        Jac(i,nm(i,12))=1;
        Jac(i,nm(i,13))=1;
    else
        Jac(i,i)=1;
        Jac(i,nm(i,7))=1;
        Jac(i,nm(i,8))=1;
        Jac(i,nm(i,12))=1;
        Jac(i,nm(i,13))=1;
    end
end
S = sparse(Jac);
