function [Pcdot,Rpt,dRpdP]=...
    matrices_AandB(t,PT,dPTdt,Pin,Pout,Pc,network_matrix,long_dist,n,fm)

deltaP = Pc-PT;
[Rpt, dRpdP] = passive_radius(t,deltaP,long_dist,n);
viscosity = zeros(n,1);
viscosity(:,1)=check_viscosity(network_matrix);
G = (pi*Rpt.^4)./(8*viscosity(:,1).*network_matrix(:,2)*1e-3);
C=2*pi*1e-3*network_matrix(:,2).*Rpt.*dRpdP; 
flow_matrix=zeros(n,1);
flow_vector=dPTdt;
Gm=zeros(n,1);
Gs = zeros(n,1);
Gs2 = zeros(size(fm.Gnt_loc,1),1);
Gsi2_t = zeros(size(fm.Gt_loc,1),1);
Gd1 =zeros(n,1);
Gd2 = zeros(n,1);
Gd3 = zeros(size(fm.Gnt_loc,1),1);
Pcm = zeros(n,1);
Pcs = zeros(n,1);
Pcs2 = zeros(size(fm.Gnt_loc,1),1);
Pcs2_t = zeros(size(fm.Gt_loc,1),1);
Pcd1 = zeros(n,1);
Pcd2 = zeros(n,1);
Pcd3 = zeros(size(fm.Gnt_loc,1),1);
Gm = G(fm.mo);
Gs = G(fm.si);
Gs2(fm.si2) = G(fm.si2index);
Gd1 = G(fm.da1);
Gd2 = G(fm.da2);
Gd3(fm.da3) = G(fm.da3index);
Gnt = G(fm.Gnt_loc);
Cnt = C(fm.Gnt_loc);
Gm_t  = G(fm.mo_t);
Gs_t = G(fm.si_t);
Gsi2_t(fm.si2_t) = G(fm.si2tindex);
G_t = G(fm.Gt_loc);
C_t = C(fm.Gt_loc);
Pcm = Pc(fm.mo);
Pcs = Pc(fm.si);
Pcs2(fm.si2) = Pc(fm.si2index);
Pcd1 = Pc(fm.da1);
Pcd2 = Pc(fm.da2);
Pcd3(fm.da3) = Pc(fm.da3index);
Pc_nt = Pc(fm.Gnt_loc);
Pcm_t = Pc(fm.mo_t);
Pcs_t = Pc(fm.si_t);
Pcs2_t(fm.si2_t)=Pc(fm.si2tindex);
Pc_t = Pc(fm.Gt_loc);

flow_matrix(fm.Gnt_loc) = (2*Gm.*Gnt.*Pcm./(Cnt.*(Gnt+Gm+Gs+Gs2))) + ...
                       (2*Gs.*Gnt.*Pcs./(Cnt.*(Gnt + Gm + Gs + Gs2)))+...
                       (2*Gs2.*Gnt.*Pcs2./(Cnt.*(Gnt + Gm + Gs + Gs2)))+...
                       (-4*Gnt.*Pc_nt./Cnt + 2*Gnt.*Gnt.*Pc_nt./(Cnt.*(Gnt + Gm + Gs + Gs2))+ ...
                          2*Gnt.*Gnt.*Pc_nt./(Cnt.*(Gnt + Gd1+ Gd2 + Gd3)))+ ...
                       (2*Gd1.*Gnt.*Pcd1./(Cnt.*(Gnt + Gd1+ Gd2 + Gd3)))+ ...
                       (2*Gd2.*Gnt.*Pcd2./(Cnt.*(Gnt+Gd1+Gd2 + Gd3)))+ ...
                      (2*Gd3.*Gnt.*Pcd3./(Cnt.*(Gnt+Gd1+Gd2 + Gd3)));
 
flow_matrix(fm.Gt_loc) = (2*Gm_t.*G_t.*Pcm_t./(C_t.*(G_t+Gm_t+Gs_t+Gsi2_t))) + ...
                       (2*Gs_t.*G_t.*Pcs_t./(C_t.*(G_t + Gm_t + Gs_t+Gsi2_t)))+...
                       (2*Gsi2_t.*G_t.*Pcs2_t./(C_t.*(G_t + Gm_t + Gs_t+Gsi2_t)))+...
                       (-4*G_t.*Pc_t./C_t + 2*G_t.*G_t.*Pc_t./(C_t.*(G_t + Gm_t+Gs_t+Gsi2_t)));
                  
 flow_matrix(1) =      (-4*G(1).*Pc(1)./C(1) + ...
                        2*G(1).*G(1).*Pc(1)./(C(1).*(G(1) + Gd1(1)+ Gd2(1))))+ ...
                       (2*Gd1(1).*G(1).*Pcd1(1)./(C(1).*(G(1) + Gd1(1)+Gd2(1))))+ ...
                       (2*Gd2(1).*G(1).*Pcd2(1)./(C(1).*(G(1)+Gd1(1)+Gd2(1))));
 
 flow_vector(fm.Gt_loc)=dPTdt(fm.Gt_loc) + (2*G_t./C_t).*Pout(fm.Gt_loc);
 flow_vector(1)=dPTdt(1)+2*G(1)*Pin/C(1); 
 
Pcdot = flow_matrix+ flow_vector;



