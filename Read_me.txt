This repository contains code for a closed-loop lumped parameter model of coupled systemic circulation of the left ventricle (LV) and coronary perfusion using a 400 vessel network. All information of the 400-vessel tree can be found in the network_matrix_test_400vessel.txt file.
Three different conditions are simulated by three main .m files namely, Modeling_CON.m, Modeling_MD.m and Modeling_MD_IS.m, respectively. 

1. In the control case, the LV pressure and volume, and flow rate waveforms can be simulated by Modeling_CON.m file and plotted by model_calibration.m file. Experimental data stored in .mat files in terms of the LV pressure and volume, flow rate waveforms used in model calibration has been included in the repository.  

2. In the mechanical dyssynchrony case, the LV pressure and volume, and flow rate waveforms can be simulated by Modeling_MD.m file. The value of SDI can be varied to adjust the degree of dyssynchrony. 

3. In the mechanical dyssynchrony + ischemia case, the LV pressure and volume, and flow rate waveforms can be simulated by Modeling_MD_IS.m file. The value of grad can be varied to adjust the gradient of linear contractility-flow relationship.

4. In sensitivity analysis, some simulations have to be run to reproduce the results. The value of SDI is varied in Modeling_MD.m file to study the effects of SDI on all variables and the value of grad is varied to study the effects of gradient of contractility-flow relationship on all variables. 